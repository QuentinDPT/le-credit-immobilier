﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditImmo.Web2.Controllers
{
    [ApiController]
    public class InvestmentController : Controller
    {
        [HttpGet("/api/investments")]
        public JsonResult GetAllInvestments()
        {
            return new JsonResult(Comportement.Views.InvestmentView.GetAllInvestments());
        }

        [HttpPost("/api/investment/compute")]
        public double ComputeInvestement(Comportement.Models.Investment.UserInvestment investment)
        {
            return Comportement.Views.InvestmentView.ComputeInvestment(investment);
        }
    }
}
