﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditImmo.Web2.Controllers
{
    [ApiController]
    public class MortgageController : Controller
    {
        [HttpPost("/api/mortgage/compute")]
        public double ComputeMortgage(Comportement.Models.Mortgage.Mortgage mortgage)
        {
            return Comportement.Views.MortgageView.ComputeMortgage(mortgage);
        }

        [HttpGet("/api/mortgage/ratetypes")]
        public JsonResult GetAllRateTypes()
        {
            return new JsonResult(Comportement.Views.MortgageView.GetAllRateType());
        }
    }
}
