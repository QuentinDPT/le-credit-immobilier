﻿using CreditImmo.Comportement.Models.Investment;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditImmo.Web.Controllers
{
    public class InvestmentController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("/api/investment/types")]
        public JsonResult GetAllInvestmentTypes()
        {
            return new JsonResult(Comportement.Views.InvestmentView.GetAllInvestments());
        }

        [HttpGet("/api/investment/{investType}")]
        public JsonResult Compute(InvestmentType investType)
        {
            return new JsonResult(Comportement.Views.InvestmentView.GetInvestment(investType));
        }
    }

}
