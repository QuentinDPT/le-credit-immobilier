import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-fetch-data',
  templateUrl: './fetch-data.component.html'
})
export class FetchDataComponent {
  public investments: Investment[];

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<Investment[]>(baseUrl + 'api/investment/types').subscribe(result => {
      this.investments = result;
    }, error => console.error(error));
  }
}

interface Investment {
  investmentName: string;
  investmentDescription: string;
  investmentType: number;
  rate: number;
  ceiling: number;
  investPeriod: number;
}
