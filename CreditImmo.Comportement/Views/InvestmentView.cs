﻿using System;
using System.Text;
using System.Collections.Generic;
using CreditImmo.Comportement.Controllers.Investment;
using CreditImmo.Comportement.Models.Investment;

namespace CreditImmo.Comportement.Views
{
    public class InvestmentView
    {
        private static InvestmentsController _investmentController { get; set; }

        private static InvestmentsController _getInvestmentController()
        {
            if (_investmentController == null)
                _investmentController = new InvestmentsController();
            return _investmentController;
        }

        public static List<Models.Investment.Investment> GetAllInvestments()
        {
            return _getInvestmentController().GetAllInvestments();
        }

        public static Models.Investment.Investment GetInvestment(Models.Investment.InvestmentType investmentType)
        {
            return _getInvestmentController().GetInvestment(investmentType);
        }

        public static double ComputeInvestment(Models.Investment.UserInvestment investment)
        {
            return _getInvestmentController().ComputeInvestment(investment);
        }
    }
}
