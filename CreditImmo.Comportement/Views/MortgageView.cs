﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreditImmo.Comportement.Views
{
    public class MortgageView
    {
        private static Controllers.Mortgage.MortgageController _mortgageController { get; set; }

        private static Controllers.Mortgage.MortgageController _getMortgageController()
        {
            if (_mortgageController == null)
                _mortgageController = new Controllers.Mortgage.MortgageController();
            return _mortgageController;
        }

        public static double ComputeMortgage(Models.Mortgage.Mortgage mortgage)
        {
            return mortgage.MonthlyPayment();
        }

        public static List<Models.Mortgage.RateType> GetAllRateType()
        {
            return _getMortgageController().GetAllRateTypes();
        }
    }
}
