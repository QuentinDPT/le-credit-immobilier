﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreditImmo.Comportement.Views
{
    class AccountView
    {
        private static Controllers.AccountController _accountController = new Controllers.AccountController();

        private static int _accountID;

        public static List<int> GetAllAccounts()
        {
            return _accountController.GetAllAccounts();
        }

        public static bool ChangeAccount(int accountID)
        {
            if (!_accountController.AccountExist(accountID))
                return false;
            _accountID = accountID;
            return true;
        }
    }
}
