﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreditImmo.Comportement.Models.Transfer
{
    public class Transfer
    {
        protected double amount { get; set; }

        protected Investment.Investment investmentSource { get; set; }

        protected Investment.Investment investmentDestination { get; set; }
    }
}
