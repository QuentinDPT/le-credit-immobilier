﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreditImmo.Comportement.Models.Mortgage
{
    public class Mortgage
    {
        public int BorrowAmount {
            get
            {
                return BorrowAmount;
            }
            set
            {
                if (value < 50000)
                    throw new ArithmeticException() ;
                BorrowAmount = value;
            }
        }

        public double AnnualRate { get; set; }

        public int Duration {
            get
            {
                return Duration;
            }
            set
            {
                if (value < 9 || value > 25)
                    throw new ArithmeticException();
                Duration = value;
            }
        }

        public double Insurance { get; set; }

        public double MonthlyPayment()
        {  
            return (BorrowAmount * (AnnualRate/12)) / (1-(1+(AnnualRate/12))-(Duration*12));
        }
    }
}
