﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditImmo.Comportement.Models.Investment
{
    public class LivretA : Investment
    {
        public LivretA()
        {
            this.InvestmentType = InvestmentType.LivretA;
            this.InvestmentName = "Livret A";
            this.InvestmentDescription = "Produit d’épargne dont le taux et le plafond sont fixés par le gouvernement";
            this.Rate = Investment.Percent(0.75);
            this.Ceiling = 22950;
            this.InvestPeriod = (int)Models.Investment.InvestPeriod.Annualy;
        }
    }
}
