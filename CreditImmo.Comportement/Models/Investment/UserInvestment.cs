﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreditImmo.Comportement.Models.Investment
{
    public class UserInvestment
    {
        public int periodInvest { get; set; }

        public double mensualInvest { get; set; }

        public double initialInvest { get; set; }

        public Models.Investment.InvestmentType investmentType { get; set; }
    }
}
