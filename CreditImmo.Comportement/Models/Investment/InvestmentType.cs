﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreditImmo.Comportement.Models.Investment
{
    public enum InvestmentType
    {
        LDD,
        LivretA,
        PEL,
        Houdini
    }
}
