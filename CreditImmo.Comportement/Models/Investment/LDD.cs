﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditImmo.Comportement.Models.Investment
{
    public class LDD : Investment
    {
        public LDD()
        {
            this.InvestmentType = InvestmentType.LDD;
            this.InvestmentName = "LDD";
            this.InvestmentDescription = "Produit d’épargne dont le taux et le plafond sont fixés par le gouvernement";
            this.Rate = Investment.Percent(0.5);
            this.Ceiling = 12000;
            this.InvestPeriod = (int)Models.Investment.InvestPeriod.Annualy;
        }
    }
}
