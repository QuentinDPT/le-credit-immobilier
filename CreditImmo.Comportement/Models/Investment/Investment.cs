﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditImmo.Comportement.Models.Investment
{
    public abstract class Investment
    {
        /// <summary>
        /// Nom de l'investissemnt
        /// </summary>
        public string InvestmentName { get; set; }

        /// <summary>
        /// Description de l'investissement
        /// </summary>
        public string InvestmentDescription { get; set; }

        /// <summary>
        /// Identifiant de l'investissement
        /// </summary>
        public InvestmentType InvestmentType { get; set; }

        /// <summary>
        /// Taux de l'investissement
        /// </summary>
        public double Rate { get; set; }

        private double _ceiling { get; set; }

        /// <summary>
        /// Plafond de l'investissement
        /// </summary>
        public double Ceiling {
            get { return _ceiling; }
            set {
                if (value < 0)
                    throw new ArithmeticException("Ceiling cannot be negative");
                _ceiling = value;
            }
        }

        private int _investPeriod { get; set; }

        /// <summary>
        /// Periode d'investissement pour toucher un revenu (en mois)
        /// </summary>
        public int InvestPeriod
        {
            get
            {
                return _investPeriod;
            }
            set
            {
                if (value <= 0)
                    throw new ArgumentException("InvestPeriod must be greater than 0");
                _investPeriod = value;
            }
        }

        public static double Percent(double percentage)
        {
            if (percentage < 0)
                throw new ArithmeticException("A percentage cannot be negative");

            return percentage * 0.01;
        }
    }
}
