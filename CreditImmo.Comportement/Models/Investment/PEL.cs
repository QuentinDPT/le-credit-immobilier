﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditImmo.Comportement.Models.Investment
{
    public class PEL : Investment
    {
        public PEL()
        {
            this.InvestmentType = InvestmentType.PEL;
            this.InvestmentName = "Plan épargne logement";
            this.InvestmentDescription = "Un plan dont le taux est aussi fixé par le gouvernement. Le capital est bloqué pendant 4 ans. Tout retrait avant les 4 ans entraine la cloture du plan.";
            this.Rate = Investment.Percent(1);
            this.Ceiling = 61200;
            this.InvestPeriod = (int)Models.Investment.InvestPeriod.Annualy;
        }
    }
}
