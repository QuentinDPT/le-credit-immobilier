﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreditImmo.Comportement.Models.Investment
{
    public enum InvestPeriod{
        Monthly = 1,
        Quarterly = 3,
        Annualy = 12
    }
}
