﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreditImmo.Comportement.Models
{
    public class Account
    {
        private int accountID {
            get
            {
                return accountID;
            }
            set { }
        }

        private List<Investment.Investment> investments { get; set; }

        public Account(int accountID)
        {
            this.accountID = accountID;
            this.investments = new List<Investment.Investment>();
        }

        public int GetAccountID()
        {
            return this.accountID;
        }
    }
}
