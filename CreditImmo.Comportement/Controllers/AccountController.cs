﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CreditImmo.Comportement.Controllers
{
    class AccountController
    {
        private List<Models.Account> accounts = new List<Models.Account>();

        private static int nextAccountID = 0;

        public int CreateAccount()
        {
            var newAccount = new Models.Account(nextAccountID++);

            accounts.Add(newAccount);

            return newAccount.GetAccountID();
        }

        public bool DeleteAccount(int accountID)
        {
            var account = GetAccount(accountID);
            if (account == null)
                return false;

            accounts.Remove(account);
            return true;
        }

        public List<int> GetAllAccounts()
        {
            return accounts.Select(x => x.GetAccountID()).ToList();
        }

        public bool AccountExist(int accountID)
        {
            return GetAccount(accountID) != null;
        }

        private Models.Account GetAccount(int accountID)
        {
            return accounts.Find(x => x.GetAccountID() == accountID);
        }
    }
}
