﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreditImmo.Comportement.Controllers.Mortgage
{
    public class MortgageCreator
    {
        public static Models.Mortgage.Mortgage CreateMortgage(int MortgageDuration, Models.Mortgage.RateType rateType)
        {
            if(MortgageDuration<=10)
            {
                switch (rateType)
                {
                    case Models.Mortgage.RateType.GOOD:
                        return new Models.Mortgage.Mortgage()
                        {
                            Duration = MortgageDuration,
                            AnnualRate = 0.67
                        };
                    case Models.Mortgage.RateType.VERY_GOOD:
                        return new Models.Mortgage.Mortgage()
                        {
                            Duration = MortgageDuration,
                            AnnualRate = 0.55
                        };
                    case Models.Mortgage.RateType.EXCEPTIONNAL:
                        return new Models.Mortgage.Mortgage()
                        {
                            Duration = MortgageDuration,
                            AnnualRate = 0.54
                        };
                }
            }
            if (MortgageDuration<=15)
            {
                switch (rateType)
                {
                    case Models.Mortgage.RateType.GOOD:
                        return new Models.Mortgage.Mortgage()
                        {
                            Duration = MortgageDuration,
                            AnnualRate = 0.85
                        };
                    case Models.Mortgage.RateType.VERY_GOOD:
                        return new Models.Mortgage.Mortgage()
                        {
                            Duration = MortgageDuration,
                            AnnualRate = 0.73
                        };
                    case Models.Mortgage.RateType.EXCEPTIONNAL:
                        return new Models.Mortgage.Mortgage()
                        {
                            Duration = MortgageDuration,
                            AnnualRate = 0.58
                        };
                }
            }
            if (MortgageDuration<=20)
            {
                switch (rateType)
                {
                    case Models.Mortgage.RateType.GOOD:
                        return new Models.Mortgage.Mortgage()
                        {
                            Duration = MortgageDuration,
                            AnnualRate = 1.04
                        };
                    case Models.Mortgage.RateType.VERY_GOOD:
                        return new Models.Mortgage.Mortgage()
                        {
                            Duration = MortgageDuration,
                            AnnualRate = 0.91
                        };
                    case Models.Mortgage.RateType.EXCEPTIONNAL:
                        return new Models.Mortgage.Mortgage()
                        {
                            Duration = MortgageDuration,
                            AnnualRate = 0.73
                        };
                }
            }

            switch (rateType)
            {
                case Models.Mortgage.RateType.GOOD:
                    return new Models.Mortgage.Mortgage()
                    {
                        Duration = MortgageDuration,
                        AnnualRate = 1.27
                    };
                case Models.Mortgage.RateType.VERY_GOOD:
                    return new Models.Mortgage.Mortgage()
                    {
                        Duration = MortgageDuration,
                        AnnualRate = 1.15
                    };
                case Models.Mortgage.RateType.EXCEPTIONNAL:
                    return new Models.Mortgage.Mortgage()
                    {
                        Duration = MortgageDuration,
                        AnnualRate = 0.89
                    };
            }

            return null;
        }
    }
}
