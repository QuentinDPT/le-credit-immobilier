﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreditImmo.Comportement.Controllers.Mortgage
{
    public class MortgageController
    {
        public List<Models.Mortgage.RateType> GetAllRateTypes()
        {
            var result = new List<Models.Mortgage.RateType>();
            result.Add(Models.Mortgage.RateType.GOOD);
            result.Add(Models.Mortgage.RateType.VERY_GOOD);
            result.Add(Models.Mortgage.RateType.EXCEPTIONNAL);

            return result;
        }
    }
}
