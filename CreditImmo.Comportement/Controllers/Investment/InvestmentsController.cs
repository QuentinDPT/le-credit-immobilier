﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreditImmo.Comportement.Controllers.Investment
{
    public class InvestmentsController
    {
        private List<Models.Investment.Investment> _allInvestments { get; set; }

        public InvestmentsController()
        {
            _allInvestments = new List<Models.Investment.Investment>();
            _allInvestments.Add(new LivretACreator().CreateInvestment());
            _allInvestments.Add(new LDDCreator().CreateInvestment());
            _allInvestments.Add(new PELCreator().CreateInvestment());
        }

        public List<Models.Investment.Investment> GetAllInvestments()
        {
            return _allInvestments;
        }

        public Models.Investment.Investment GetInvestment(Models.Investment.InvestmentType investmentType)
        {
            return _allInvestments.Find(x => x.InvestmentType == investmentType);
        }

        public double ComputeInvestment(Models.Investment.UserInvestment investment)
        {
            var inv = GetInvestment(investment.investmentType);

            double capital = (investment.initialInvest < inv.Ceiling ? investment.initialInvest : inv.Ceiling);

            for (var i = 1; i <= investment.periodInvest; i++)
            {
                if (i % inv.InvestPeriod == 0)
                    capital = capital + ((capital < inv.Ceiling ? capital : inv.Ceiling) * inv.Rate);

                if(capital + investment.mensualInvest < inv.Ceiling)
                    capital += investment.mensualInvest;
            }

            return capital;
        }
    }
}
