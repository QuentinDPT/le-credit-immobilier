﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreditImmo.Comportement.Controllers.Investment
{
    public class LDDCreator : InvestmentCreator
    {
        public override Models.Investment.Investment CreateInvestment()
        {
            return new Models.Investment.LDD();
        }
    }
}
