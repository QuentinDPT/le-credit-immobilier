﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreditImmo.Comportement.Controllers.Investment
{
    public abstract class InvestmentCreator
    {
        public abstract Models.Investment.Investment CreateInvestment();
    }
}
